# Control BootCamp - Steve Brunton

Files:
control_bootcamp_code.zip - contains the base code not modified.
control_bootcamp_code (Folder) - contains the code maybe modified by me.

You can find the greatest bootcamp on control in the next link:
https://www.youtube.com/playlist?list=PLMrJAkhIeNNR20Mz-VpzgfQs5zrYi085m

Book:
These lectures follow Chapters 1 & 3 from:

Machine learning control, by Duriez, Brunton, & Noack